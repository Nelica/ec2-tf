resource "aws_internet_gateway" "ec2-env-gw" {
  vpc_id = "${aws_vpc.ec2-env.id}"
tags = {
    Name = "ec2-env-gw"
  }
}
