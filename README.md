# Ec2 Tf

Instalacija AWS cli-a i konfiguracija Access ID i secret access key. 

https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

```bash
AWS configure

Kredencijali se nalaze u Security Credentials na AWS konzoli. 
```

Uraditi git clone u vas direktorijum

```bash
git clone https://gitlab.com/Nelica/ec2-tf.git
```
Podesite sledece varijable:

```bash
connections.tf : Region
security.tf : Security grupa i portovi ( ako zelite dodatne portove, default 22)
gateway.tf : Gateway VPC-a
server.tf : Ime instance, size instance, key pair i AMI id koji vuce iz variables.tf 
variables.tf : Predefenisane variable, obrisati default vrednosti za prompt
```

Nakon uspesne konfiguracije, koristite:

```bash
terraform init 
terraform plan
terraform apply
```

Da kreirate SSH kljuc preko TF ( ako nemate vec kreiran keypair svoj na AWS), dodajte sledeci resurs:
```bash
resource "aws_key_pair" "deploy" {
  key_name   = "Terraform-test"
  public_key = "ssh-rsa javnikljuc"
}
```


