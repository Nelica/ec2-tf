variable "ami_name" {
  description = "Opis instance"
  type        = string
  default     = "ime-instance"
}


variable "ami_id"  {
  description = "ID image-a"
  type        = string
  default     = "ami-ovde"
}

variable "ami_key_pair_name"  {
  description = "Kljuc"
  type        = string
  default     = "Vas-kljuc"	
}
