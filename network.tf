resource "aws_vpc" "ec2-env" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "ec2-env"
  }
}

resource "aws_eip" "ec2-env" {
  instance = "${aws_instance.eks-ec2-instance.id}"
  vpc      = true
}
