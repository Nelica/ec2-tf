resource "aws_subnet" "subnet-uno" {
  cidr_block = "${cidrsubnet(aws_vpc.ec2-env.cidr_block, 3, 1)}"
  vpc_id = "${aws_vpc.ec2-env.id}"
  availability_zone = "eu-central-1a"
}

resource "aws_route_table" "route-table-ec2-env" {
  vpc_id = "${aws_vpc.ec2-env.id}"
route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.ec2-env-gw.id}"
  }
tags = {
    Name = "ec2-env-route-table"
  }
}
resource "aws_route_table_association" "subnet-association" {
  subnet_id      = "${aws_subnet.subnet-uno.id}"
  route_table_id = "${aws_route_table.route-table-ec2-env.id}"
}
